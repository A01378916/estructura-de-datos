package mx.itesm.exercises;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Exam2 {
	public static int max(List<Integer> list) {
		
	//MANERA NO RECURSIVA
		
		/*if(list.isEmpty()) {
		return Integer.MAX_VALUE;
	}
	
	int max=list.get(0);
	for(int i = 0; i < list.size(); i++) {
		if(max<list.get(i)) {
			max = list.get(i);
		}
	}
	return max;*/
	
	if(list.isEmpty()) {
		return Integer.MIN_VALUE;
	}
	
	if(list.size() == 2) {
		return Math.max(list.get(0), list.get(1));
	}
	
	if(list.size()  < 2) {
		return list.get(0);
	}
	
	int divider = list.size()/2;
	//Listas para guardar los valores 
	List<Integer> after = new LinkedList<Integer>();
	List<Integer> before = new LinkedList<Integer>();
	
	//Agregamos los elementos menores al divider
	for(int i = 0; i < divider; i++) {
		after.add(list.get(i));
	}
	//Agregamos los elementos mayores al divider
	for(int i = divider; i < list.size(); i++) {
		before.add(list.get(i));
	}
	
	//Aplicamos recursividad para ambas listas
	return Math.max(max(after), max(before));
}
	
	
	public static int maxSymbolDepth(String exp, char opening, char closing) {
		//Primera condici�n 
		if(opening == closing) {
			return -1;
		}
		
		Deque<Character> characters = new LinkedList<Character>();
		LinkedList<Integer> count = new LinkedList<Integer>();
		int index = 0;
		int i = 0;
		
		while(i < exp.length()) {
			if(characters.isEmpty() && i < exp.length()) {
				count.add(index);
				index = 0;
			}
			if(exp.charAt(i) == opening) {
				characters.push(exp.charAt(i));
			}
			else if(exp.charAt(i) == closing) {
				if(!characters.isEmpty()) {
					index++;
					characters.pop();
				}
				else {
					return -1;
				}
			}
			i++;
		}
		//Si a�n quedan elementos en la lista
		if(!characters.isEmpty()) {
			return -1;
		}
		else {
		count.add(index);
		//Metodo creado previamente
		return max(count);
		}
	}
	
	public static String toBinary(int n) {
		//NO RECURSIVO
		/*String s = "";
		while (n > 0) {
			if (n%2 ==0) {
				s = "0" +s;
				n= n / 2;
			}
			else {
				s = "1"+s;
				n= n/2;
			}
		}
		return s;*/
		
		//RECURSIVO
		if(n<=0) {
			return "";
		}
		return toBinary(n/2) + (n%2);
	}
}